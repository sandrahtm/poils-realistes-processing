PImage img, img2;
PGraphics img3;

int col = color(255,0,0);
int selpix = 0;
int iniMouseX, iniMouseY;
float minr, maxr, ming, maxg, minb, maxb;
float minh, maxh, mins, maxs, minbr, maxbr;
int minx, miny, maxx, maxy;
int minx0, miny0, maxx0, maxy0;
ArrayList<PVector> lastMousePoints = new  ArrayList<PVector>();
boolean alt = false;
PVector ortho = new PVector(5, 10);
boolean is_check_1 = false;
boolean is_check_2 = false;
boolean is_check_3 = false;
int nb_de_poils_user = 50;

void setup() {
  size(600,600); //820,761
  // l'image de depart
  img = loadImage("skunk.png");
  img.loadPixels();
  
  // pour dessiner la zone selectionnee
  img2 = createImage(img.width, img.height, ARGB);
  img2.loadPixels();
  
  // pour dessiner les poils
  img3 = createGraphics(img.width, img.height);
  pixelDensity(displayDensity());
  background(255);
  frameRate(10);
}


void draw() {
  background(255);
  noTint();
  
  
  image(img,0,0);
  //etape 2.3
  // ici on peut jouer sur la teinture de l'image pour faire une
  // animation moins binaire
  if(frameCount%20<10) {
    tint(255, 192, 203, 64);
  }else {
    tint(199, 24, 133, 64);
  }
  image(img2,0,0); 

  noTint();
  image(img3,0,0);
  
  /* Couleur du texte et des rectangles */
  fill(255, 25, 250);
  
  /*Définition des textes*/
  text("taille variable : ", 10, img.height - 72);
  text("épaisseur variable : ", 10, img.height - 50);
  text("luminosité variable : ", 10, img.height - 28);
  text("nb poils (10-90) : ", 10, img.height - 8);
  
  /*Animation des rectangles "cochées" ou non*/
  
  if (is_check_3) {
    rect(150,img.height-87,20,20);
    stroke(255,255,0);
    line(152,img.height-85,168,img.height-69);
    line(168,img.height-85,152,img.height-69);
    stroke(0,0,0);
  }
  else {
    rect(150,img.height-87,20,20);
  }
  
  if (is_check_1) {
    fill(255, 70, 250);
    rect(150,img.height-65,20,20);
    stroke(255,255,0);
    line(152,img.height-63,168,img.height-47);
    line(168,img.height-63,152,img.height-47);
    stroke(0,0,0);
  }
  else {
    rect(150,img.height-65,20,20);
  }
  
  if (is_check_2) {
    rect(150,img.height-43,20,20);
    stroke(255,255,0);
    line(152,img.height-41,168,img.height-25);
    line(168,img.height-41,152,img.height-25);
    stroke(0,0,0);
  }
  else {
    rect(150,img.height-43,20,20);
  }
  
  /*Rectangles + et -*/
  
  rect(120,img.height-19,15,15);
  rect(170,img.height-19,15,15);
  fill(0,0,0);
  text("-", 126,img.height - 7);
  text("+",174,img.height - 7);
  text(nb_de_poils_user,145,img.height-7);
}

// pour les macs
void keyPressed() {
  if (key == CODED && keyCode == ALT)
    alt=true;
}

void keyReleased() {
  if (key == CODED && keyCode == ALT)
    alt=false;
}


void mousePressed(){
  
  /*Si le bouton gauche est pressée et que l'utilisateur clique sur un des rectangles, la variable booléenne correspondante change de valeur*/
  
  if ((mouseButton == LEFT) && (mouseX > 150) && (mouseX < 170) && (mouseY > img.height - 65) && (mouseY < img.height - 45)) {
    is_check_1 = !is_check_1;
  }
  
  if ((mouseButton == LEFT) && (mouseX > 150) && (mouseX < 170) && (mouseY > img.height - 43) && (mouseY < img.height - 21)) {
    is_check_2 = !is_check_2;
  }
  
  if ((mouseButton == LEFT) && (mouseX > 150) && (mouseX < 170) && (mouseY > img.height - 87) && (mouseY < img.height - 67)) {
    is_check_3 = !is_check_3;
  }
  
  if ((mouseButton == LEFT) && (mouseX > 120) && (mouseX < 135) && (mouseY > img.height - 19) && (mouseY < img.height - 4) && nb_de_poils_user > 10) {
    nb_de_poils_user -= 10;
  }
  
  if ((mouseButton == LEFT) && (mouseX > 170) && (mouseX < 185) && (mouseY > img.height - 19) && (mouseY < img.height - 4) && nb_de_poils_user < 90) {
    nb_de_poils_user += 10;
  }
  
  
  if (mouseButton == LEFT) {
    // si on commence un drag & drop avec le bouton gauche, on selectionne 
    iniMouseX = -1;
    lastMousePoints.clear();
    col = img.pixels[mouseX+mouseY*img.width];

    // on recommence toute la selection  donc on remet a zero toutes les variables
    // qui concernent les stats de la selection
    maxr = minr = red(col);
    maxg = ming = green(col);
    maxb = minb = blue(col);
    minh = hue(col);
    maxh = hue(col);
    mins = saturation(col);
    minx = maxx = mouseX;
    miny = maxy = mouseY;
    minx0 = maxx0 = mouseX;
    miny0 = maxy0 = mouseY;
    
    selpix = 1;
    // on recommence toute la selection donc on remet tous les pixels transparents
    for(int j=0; j<img.height; j++)
      for(int i=0; i<img.width; i++){
        img2.pixels[i+j*img2.width] = color(0,0,0, 0);
      }
    img2.pixels[mouseX+mouseY*img2.width] = color(0,0,0);
  }
  if (mouseButton == LEFT || (mouseButton == RIGHT && alt)){
    // si on commence un drag & drop avec le bouton droit (ou avec la touche alt pour les macs)
    iniMouseX = mouseX;
    iniMouseY = mouseY;
  }
}

void mouseDragged(){
  if (mouseButton == LEFT){
    // on rajoute un point de drag&drop dans la selection
    col = img.pixels[mouseX+mouseY*img.width];
    maxr = minr = red(col);
    maxg = ming = green(col);
    maxb = minb = blue(col);
    minh = hue(col);
    maxh = hue(col);
    mins = saturation(col);
    minx = maxx = mouseX;
    miny = maxy = mouseY;
    
    // on ne garde en memoire que les 25 derniers points pour pas trop 
    // rajouter de travail
    if (lastMousePoints.size()>25)
      lastMousePoints.remove(lastMousePoints.size()-1);
    lastMousePoints.add(0,new PVector(mouseX, mouseY, col));
    
    // pour les 25 derniers points on met a jour les stats
    for(PVector v:lastMousePoints){
      int x0 = int(v.x);
      int y0 = int(v.y);
      col = img.pixels[x0+y0*img.width];
      minr = min(minr, red(col));
      maxr = max(maxr, red(col));
      ming = min(ming, green(col));
      maxg = max(maxg, green(col));
      minb = min(minb, blue(col));
      maxb = max(maxb, blue(col));
      
      minh = min(minh, hue(col));
      maxh = max(maxh, hue(col));
      mins = min(mins, saturation(col));
      maxs = max(maxs, saturation(col));
      minbr = min(minbr, brightness(col));
      maxbr = max(maxbr, brightness(col));
      
      minx = min(minx, x0);
      maxx = max(maxx, x0);
      miny = min(miny, y0);
      maxy = max(maxy, y0);
      
      img2.pixels[x0+y0*img2.width] = color(0,0,128); 
      selpix++;      
    }
   
    // en fonction des stats on selectionne tous les points autour qui
    // ressemblent (memes couleurs)
    int iniselpix = selpix-1;
    while(iniselpix!=selpix){
      // on recommence en boucle tant que ca rajoute des points
      iniselpix = selpix;
      selpix = 0;
      for(int j=max(0,miny-1); j<=min(maxy+1,img.height-1); j++)
        for(int i=max(0,minx-1); i<=min(maxx+1,img.width-1); i++){
          int p = img.pixels[i+j*img.width];
          boolean tsel = j>miny-1 && j>0 && alpha(img2.pixels[i+(j-1)*img2.width])>128;
          boolean bsel = j<maxy+1 && j<img.height-1 && alpha(img2.pixels[i+(j+1)*img2.width])>128;
          boolean lsel = i>minx-1 && i>0 && alpha(img2.pixels[(i-1)+j*img2.width])>128;
          boolean rsel = i<maxx+1 && i<img.width-1 && alpha(img2.pixels[(i+1)+j*img2.width])>128;
          
          float avgr = (minr+maxr)/2, gapr = (maxr-minr)/2;
          float avgg = (ming+maxg)/2, gapg = (maxg-ming)/2;
          float avgb = (minb+maxb)/2, gapb = (maxb-minb)/2;
          
          float avgh = (minh+maxh)/2, gaph = (maxh-minh)/2;
          float avgs = (mins+maxs)/2, gaps = (maxs-mins)/2;
          float avgbr = (minbr+maxbr)/2, gapbr = (maxbr-minbr)/2;
          
          float tol = 1.0+((tsel?0.05:0) + (bsel?0.05:0) + (lsel?0.05:0) + (rsel?0.05:0));
          if (red(p)  >=avgr-gapr*tol && red(p)  <=avgr+gapr*tol &&
              green(p)>=avgg-gapg*tol && green(p)<=avgg+gapg*tol &&
              blue(p) >=avgb-gapb*tol && blue(p) <=avgb+gapb*tol && 
              hue(p)        >=avgh-gaph*tol && hue(p)        <=avgh+gaph*tol &&
              saturation(p) >=avgs-gaps*tol && saturation(p) <=avgs+gaps*tol &&
              brightness(p) >=avgbr-gapbr*tol && brightness(p) <=avgbr+gapbr*tol && 
              (tsel || bsel || lsel || rsel)){
            img2.pixels[i+j*img2.width] = color(avgr,avgg,avgb); //etape2.1
            selpix++;
            minx = min(minx, i);
            maxx = max(maxx, i);
            miny = min(miny, j);
            maxy = max(maxy, j);
            minx0 = min(minx0, i);
            maxx0 = max(maxx0, i);
            miny0 = min(miny0, j);
            maxy0 = max(maxy0, j);            
          }
        }      
    }
    
    //etape2.2
    // ici on peut bidouiller les couleurs des pixels selectionnes
    // en fonction de leur position ou de leur voisin
    for (int j=1; j<img2.height-1; j++)
      for (int i=1; i<img2.width-1; i++){
        if (alpha(img2.pixels[i+j*img2.width])>128)
          img2.pixels[i+j*img2.width] = color(0,50,100,200); //Pour faire une subrillance rose : color(255,50,0,200)
      }
      
    img2.updatePixels();
    
  }
}

void mouseReleased(){
  
  /*Si mouseReleased provient d'un clique gauche (i.e. d'un cochement de rectangle), on ne fait rien*/
  
  if (mouseButton == RIGHT) return;
  
  /*On récupère les coordonnées de la souris puis on définit p comme le vecteur initial de direction des poils*/
  
  int finalmouseX = mouseX;
  int finalmouseY = mouseY;
  PVector p = new PVector(finalmouseX-iniMouseX,finalmouseY-iniMouseY); 
  
  /*On prend comme couleur la couleur du pixel du clic initial*/
  
  col = img.pixels[iniMouseX+iniMouseY*img.width];
  
  /*Calcul de la magnitude*/
  
  float magnitude = dist(iniMouseX,iniMouseY,finalmouseX,finalmouseY);
  
  /*Compteur du nombre de poils dessinés*/
  
  int compteurdepoils = 0;
  
  /*On récupère le nombre de poils demandé par l'utilisateur*/
  
  int nbdepoils = nb_de_poils_user;
  img3.beginDraw();
  img3.stroke(col);
  
  /*Si l'utilisateur choisi de faire varier l'épaisseur des poils, on change l'attribut strokeWeight des poils en fonction de la magnitude*/
  
  if (is_check_1) img3.strokeWeight( log(magnitude) - 1 );
  else img3.strokeWeight(1);
  
  /*On fait varier la longueur des poils selon la magnitude dans le cas où l'utilisateur coche la case correspondante, sinon on la fixe à 50*/
  
  int longueur_poil = 50;
  if (is_check_3) longueur_poil = (int)magnitude;
  
  /*Pour ne pas avoir des poils trop long, on doit imposer une norme au vecteur directionnelle, ici égale à 2*/
  
  p.normalize().mult(2);
  
  /*Création des poils, la boucle se termine puisque compteurdepoils est incrémenté à chaque tour de boucle*/
  
  while (compteurdepoils<nbdepoils) {
    
    /*new_p vaut p à chaque début de création d'un nouveau poil*/
    
    PVector new_p = new PVector(p.x,p.y);
    int x0 = (int)random(img.width);
    int y0 = (int)random(img.height);
    while (alpha(img2.pixels[x0+y0*img.width]) <= 128) {
      x0 = (int)random(img.width);
      y0 = (int)random(img.height);
    }
    
    //Dessiner un trait partant du pixel x0,y0 
    
    /*On part du vecteur orthogonal à p, que l'on modifie aléatoirement*/
    
    PVector ortho_actu = new PVector(-new_p.y,new_p.x);
    ortho_actu.normalize().mult(2);
    ortho_actu.mult(random(1.5)-0.75);
    
    /*On ajoute à notre p le vecteur orthogonal modifié*/
    
    new_p.add(ortho_actu);
    
    /*On fait une boucle pour définir chaque partie du poils, (xact,yact) est le point actuel et (xsuiv,ysuiv) est le point d'arrivé de chaque petit poil*/
    
    int xact = x0;
    int yact = y0;
    int xsuiv = x0 + (int)new_p.x;
    int ysuiv = y0 + (int)new_p.y;
    img3.line(xact,yact,xsuiv,ysuiv);
    int opacity = 255;
    for (int i = 0; i < longueur_poil ; i++) {
      
      /*Si l'utilisateur le décide, on modifie linéairement l'opacité du poil en fonction de la partie du poil sur laquelle on se trouve*/
      
      if (is_check_2) {
        if (i!=0) opacity = ((-255 / longueur_poil) * i + 255);
        img3.stroke(col,opacity);
      }
      
      /*On applique le même principe qu'initialement, pour chaque partie du poil*/
      
      ortho_actu = new PVector(yact-ysuiv,xsuiv-xact);
      ortho_actu.normalize().mult(0.7);
      ortho_actu.mult(random(1.5)-0.75);
      new_p.add(ortho_actu);
      xact = xsuiv;
      yact = ysuiv;
      xsuiv = xact + (int)new_p.x;
      ysuiv = yact + (int)new_p.y;
      img3.line(xact,yact,xsuiv,ysuiv);
      
    }
    compteurdepoils+=1;    
  }
  img.updatePixels();
  img3.endDraw();
}
